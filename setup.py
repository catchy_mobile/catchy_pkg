from setuptools import find_packages
from setuptools import setup

# PyPI dependencies should be put here
requirements = """
pip>=9
setuptools>=26
wheel>=0.29
pandas
yapf
flake8
coverage
xlrd
"""
# Private dependencies should be handled by requirements.txt
#
# If you really want to install private dependency automatically
# when the package is installed, use
#
#    dependency_links = ['vcs+proto://host/path@revision#egg=project-version']
#
# key for `setup`.
#
# E.g.
#
#    dependency_links = ['git+ssh://git@gitlab.zettafox.com/r_d/ble@9.1#ble-9.1']
#
# and then use `ble==9.1` in requirements. You can install only specific
# version in that way. Use this option only when you are sure that its a
# right way to do in your project.

setup(name='catchy_pkg',
      setup_requires=['setuptools_scm'],
      use_scm_version=True,
      description="Skeleton for data science & machine learning projects",
      packages=find_packages(),
      test_suite='tests',
      install_requires=requirements,
      # include_package_data: to install data from MANIFEST.in
      include_package_data=True,
      scripts=['scripts/cy_init_index',
               'scripts/cy_inject_sample_data',
               'scripts/classify_image'],
      zip_safe=False)
