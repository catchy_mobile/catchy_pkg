# -*- coding: UTF-8 -*-
# Copyright (C) 2018 Catchy Louis Cassedanne <louiscassedanne@gmail.com>
""" Main lib for catchy_pkg Project
"""

# Import from standard Library
import pandas as pd
import sys
import json

# Import from catchy_pkg and others


pd.set_option('display.width', 200)


def read_xlsx(path):
    """
    Read an xlsx file and return a list of dict
    """
    df = pd.read_excel(path)
    keys = df.columns.tolist()
    data = []
    for i, row in df.iterrows():
        d = {}
        for key in keys:
            d[key] = row[key]
        data.append(d)
    return data


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")
    return True


def read_json_file(path):
    json_data = open(path).read()
    data = json.loads(json_data)
    return data


if __name__ == '__main__':
    print('FILLME')
