#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Copyright (C) 2018 Catchy
"""
"""

# Import from Standard Library
from elasticsearch import Elasticsearch
from os import environ

# Import from catchy_pkg and others

env_var = environ.get("ELASTICSEARCH_URL", "http://localhost:9200")
cnx = Elasticsearch(env_var, timeout=600)


def get_mapping():
    """Defines mapping for different index"""
    mapp = {"mappings": {
                "catchy": {
                    "properties": {
                        'type': {'type': 'keyword'},
                        'creation_date': {'type': 'date'},
                        'user_id': {'type': 'keyword'},
                        'content': {'type': 'text'},
                        'city': {'type': 'keyword'},
                        'location': {'type': 'geo_point'},
                        'file_path': {'type': 'text'},
                        'category': {'type': 'keyword'},
                        'email': {'type': 'text'},
                        'tel': {'type': 'text'},
                        'nickname': {'type': 'text'},
                        'postcode': {'type': 'integer'},
                        'hiden_phone_id': {'type': 'keyword'}}}}}
    return mapp
