# !/usr/bin/env python
# -*- coding: UTF-8 -*-
# Copyright (C) 2018 Catchy

# Import from Standard Library

# Import from catchy_pkg and others
from catchy_pkg.es_mapping import cnx, get_mapping
from catchy_pkg.lib import query_yes_no


def delete_index(cnx, index='catchy'):
    """ delete an index"""
    try:
        cnx.indices.delete(index, ignore=400)
        print('{} index deleted !'.format(index))
    except Exception:
        print('{} not found!'.format(index))


def create_index(cnx, index='catchy', force=False):
    """Creates and set mapping for a given index"""
    if force:
        mapping = get_mapping()
        if cnx.indices.exists(index):
            delete_index(cnx, index)
        cnx.indices.create(index=index, body=mapping)
        print('==> "{}" index successfully reset !'.format(index))
    else:
        question = 'Do you really want to reset "{}" index?'
        q = query_yes_no(question.format(index))
        if q:
            mapping = get_mapping()
            if cnx.indices.exists(index):
                delete_index(cnx, index)
            cnx.indices.create(index=index, body=mapping)
            print('==> "{}" index successfully reset !'.format(index))
        else:
            print('==> "{}" index not reset.')


def get_type_data(cnx, es_type='users', index='catchy'):
    """
    """
    body = {"query": {"match": {"type": es_type}}}
    res = cnx.search(index=index, doc_type=index, body=body, size=1000)
    data = res['hits']['hits']
    return data


def get_categories(cnx, index='catchy'):
    """
    Returns a list of categories used for comment categorization
    """
    coms_data = get_type_data(cnx, 'comments', index=index)
    cats = [d['_source']['category'] for d in coms_data]
    categories = list(set(cats))
    return categories


def get_category_coms(cnx, cat, size, index='catchy'):
    """
    Returns all comments from a specific category.
    E.g. all the "infrastructures sportives" comments
    """
    body = {"query": {"bool": {"must": [{"match": {"type": 'comments'}},
                               {"match": {"category": cat}}]}}}
    res = cnx.search(index=index, doc_type=index, body=body, size=size)
    data = res['hits']['hits']
    return data


if __name__ == '__main__':
    create_index(cnx)
