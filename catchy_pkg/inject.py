# !/usr/bin/env python
# -*- coding: UTF-8 -*-
# Copyright (C) 2018 Catchy

# Import from Standard Library
from elasticsearch import helpers

# Import from catchy_pkg and others


def inject_data(cnx, data, index='catchy'):
    """
    inject comments data in ES. Data is a list of dict where
    each dict is a comment data
    data = [{
        "content": "Plus de communication de la mairie" ,
        "creation_date": "2018-03-19",
        "city": "Suresnes",
        "user_id": 1]
    """
    helpers.bulk(cnx, data, index=index, doc_type=index)
    cnx.indices.flush(index)
    print('==> Injection : SUCCESS\n')


if __name__ == '__main__':
    print('ok')
