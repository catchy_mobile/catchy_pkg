import os
from pkg_resources import resource_filename

import pandas as pd


# The correct way to access data files is through
# pkg_resources.resource_filename
def dataset(filename="data.csv.gz"):
    """ Returns default data from package """
    src = resource_filename(__name__, os.path.join("data", filename))
    data = pd.read_csv(src)
    return data
