# -*- coding: UTF-8 -*-
# Copyright (C) 2018 Catchy Louis Cassedanne <louiscassedanne@gmail.com>
"""NLP lib for catchy_pkg Project"""

# import spacy
import progressbar
from textblob import TextBlob

# import from catchy_pkg
from catchy_pkg.query import get_categories
from catchy_pkg.query import get_category_coms


def lemmatize_strict(body, nlp):
    """ lemmatize token"""
    body = body.lower()
    doc = nlp(body)
    list_lemma = []
    for token in doc:
        cond1 = not token.is_punct
        cond2 = not token.is_space
        cond3 = token.is_alpha
        cond4 = not token.lemma_ == '-PRON-'
        cond5 = not token.like_num
        cond6 = not token.is_digit
        cond7 = len(token) > 1
        cond8 = not token.is_stop
        cond9 = token.is_ascii
        if (cond1 and cond2 and cond3 and cond4
                and cond5 and cond6 and cond7 and cond8
                and cond9):
            list_lemma.append(token.lemma_)
    return ' '.join(list_lemma)


def lemmatize(body, nlp):
    """ lemmatize token"""
    body = body.lower()
    doc = nlp(body, disable=['ner', 'parser'])
    list_lemma = []
    for token in doc:
        conditions = {}
        conditions['cond2'] = not token.is_punct
        conditions['cond3'] = not token.is_space
        conditions['cond4'] = not token.lemma_ == '-PRON-'
        conditions['cond5'] = not token.is_digit
        conditions['cond6'] = len(token) > 1
        conditions['cond8'] = token.pos_ in ['VERB', 'NOUN',
                                             'ADJ', 'PROPN',
                                             'ADV', 'X']
        if all(conditions.values()):
            list_lemma.append(token.lemma_)
    return ' '.join(list_lemma)


def get_all_entities(body, nlp):
    """
    Should return a dict
    d = {"loc_ent": ["Parc du Château", ...],
         "org_ent": ["ONU", ...],
         ...}
    """
    ent_list = ['LOC', 'PERSON', 'NORP', 'FAC', 'ORG', 'GPE', 'PRODUCT',
                'EVENT', 'WORK_OF_ART', 'LAW', 'LANGAGE', 'DATE',
                'TIME', 'PERCENT', 'MONEY', 'QUANTITY', 'CARDINAL', 'ORDINAL']
    doc = nlp(body)
    d = {}
    for ent in ent_list:
        d['{}_ent'.format(ent)] = ent_recognizer(doc, ent)
    return d


def ent_recognizer(doc, ent_name):
    ents = [(ent.string, ent.label_) for ent in doc.ents
            if ent.label_ == ent_name]
    return ents


def coms_data_process(data, nlp):
    print("   Comments data pre-processing :")
    bar = progressbar.ProgressBar()
    for com in bar(data):
        # Lemmatization
        com['content_lem'] = lemmatize(com['content'], nlp)
        # NER
        entities_dict = get_all_entities(com['content'], nlp)
        com.update(entities_dict)
    return data


def sent_analysis(statement):
    """
    Takes a comment (sentence) as input, and returns a number
    (from 0 to 1) describing polarity
    """
    sentiment = TextBlob(statement)
    return sentiment.sentiment.polarity


def get_categ_similarity(cnx, content, nlp):
    """
    returns a dict containing the average of similarity
    between a comment and each category
    """
    cats = get_categories(cnx)
    lem_content = lemmatize(content, nlp)
    res = {}
    doc = nlp(lem_content)
    for cat in cats:
        ref_coms = get_category_coms(cnx, cat, 50)
        coms = [d['_source']['content'] for d in ref_coms]
        sims = [doc.similarity(nlp(com)) for com in coms]
        score = sum(sims)/len(sims)
        res[cat] = score
    return res


if __name__ == '__main__':
    print('ok')
