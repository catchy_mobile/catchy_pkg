# -*- coding: UTF-8 -*-
# Copyright (C) 2018 Catchy Louis Cassedanne <louiscassedanne@gmail.com>
""" Image processing lib for catchy_pkg Project
"""
# Import from standard Library
import base64

# Import from catchy_pkg and other


def encode_image(image_path):
    with open(image_path, 'rb') as image_file:
        encoded_string = base64.b64encode(image_file.read())
    return encoded_string


def decode_image(encoded_string):
    data = base64.b64decode(encoded_string)
    return data


if __name__ == '__main__':
    print('hello world')
