
Data analysis
=============

-  Project: name
-  Description: Skeleton for data science & machine learning projects
-  Data Source:
-  Type of analysis:

Please document the project the best you can.


Install Elasticsearch
---------------------
You will need to have Elasticsearch installed on your laptop to use catchy_pkg 
locally.

If you are on macOS, you will first need to have Java 1.8:: 
  $ brew update
  $ brew cask install caskroom/versions/java8

And then get elasticsearch::
  $ brew install elasticsearch

To have launched start elasticsearch::
  $ brew services start elasticsearch

Note on Elasticsearch version
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Important updates have been made between 5.0 and 6.0 version:
The different mapping types inside an index have been removed. For example,
it is now impossible to have an index called "Twitter" with 2 types "Tweet" and "Users" in it.

2 solutions to handle this update:
- Create a specific index corresponding to each type (a "tweet" index and a "user" index) --> single type in each index should be name "_doc"
- In the mapping of the index "Twitter", define a "type" field (keyword type) common to all documents.
  
Continuous integration
----------------------

Every push of master branch will execute ``.gitlab-ci.yml`` docker jobs.
By default it will run tests, make a wheel and deploy it ``kvm05`` test
server.

Bear in mind that Docker image used for deployment is very basic: it has
Ansible to run playbook and ssh to connect. Any builds in CI/CD workflow
should be done in stages prior to deploy with results registered as
artifacts.

NLP applications
----------------
This project runs NLP analysis and so does it require several python libraries to do it.

Spacy
~~~~~
Spacy is automatically installed when running ``pip install -r requirements``, but you need then
to download the required models. For example, to get the 'fr' model, run::

    python -m spacy download fr

To get a list of all existing models on Spacy, have a look at : https://spacy.io/models/
