clean:
	@rm -f */version.txt
	@rm -f .coverage
	@rm -fr */__pycache__
	@rm -fr __pycache__
	@rm -fr build
	@rm -fr dist
	@rm -fr catchy_pkg-*.dist-info
	@rm -fr catchy_pkg.egg-info

all: clean install test check_code

install: clean wheel
	@pip3 install -U dist/*.whl

dev_install:
	@pip3 install -e .

wheel: clean
	@python3 setup.py bdist_wheel

prod_install: wheel
	@echo "Wheel is made. Use ansible-playbook to deploy: "
	@echo "    ansible-playbook -i ansible/all.serverlist  \
						                  --extra-vars "venv=venv"   \
						                  ansible/playbook_deploy.yml"

check_code:
	@flake8 scripts/* catchy_pkg/*.py tests/*.py

test:
	@coverage run -m unittest tests/*.py
	@coverage report -m --omit=$(VIRTUAL_ENV)/lib/python*,catchy_pkg/*

ftest:
	@cd /tmp; cy_init_index -f
	@cy_inject_sample_data

uninstal:
	@python3 setup.py install --record files.txt
	@cat files.txt | xargs rm -rf
	@rm -f files.txt

count_lines:
	@find ./ -name '*.py' -exec  wc -l {} \; | sort -n| awk \
        '{printf "%4s %s\n", $$1, $$2}{s+=$$0}END{print s}'
	@echo ''
	@find ./scripts -name '*-*' -exec  wc -l {} \; | sort -n| awk \
		        '{printf "%4s %s\n", $$1, $$2}{s+=$$0}END{print s}'
	@echo ''
	@find ./tests -name '*.py' -exec  wc -l {} \; | sort -n| awk \
        '{printf "%4s %s\n", $$1, $$2}{s+=$$0}END{print s}'
	@echo ''
