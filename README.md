![MasterBuild](https://gitlab.zettafox.com/2017/catchy_pkg/badges/master/build.svg)\_
![MasterCoverage](https://gitlab.zettafox.com/2017/catchy_pkg/badges/master/coverage.svg?job=coverage)\_
![LicenseMIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)\_

Data analysis
=============

-   Project: name
-   Description: Skeleton for data science & machine learning projects
-   Data Source:
-   Type of analysis:

Please document the project the best you can.

Start-up the project
--------------------

**Remove this section when you are done with setting up the project.**

Welcome, this is you brand new data science/machine learning project!
Almost everything is set up here for you. Please follow this simple
guide to know what next you should do.

First, follow the [Installation](#installation) instructions to install your
project locally for development.

Next, check if your project is in correct group in our 
[gitlab](https://gitlab.zettafox.com). If your project is not there please add it:

-   Create a new project in proper group.
-   Populate it:

        $ # if group is "infra" and project_name is "catchy_pkg"
        $ git remote add origin git@gitlab.zettafox.com:2017/catchy_pkg.git
        $ git push -u origin master
        $ git push -u origin --tags

You will find following components prepared for you:

### Basic python package

A very basic python package with exemplary data file and cleaning
function is in `catchy_pkg` path:

    catchy_pkg
    ├── data             # data directory
    │   └── data.csv.gz  # exemplary data
    ├── __init__.py      # this define a package
    ├── lib.py           # here you put you code
    └── logger.py        # logging facilities

    1 directory, 4 files

You will most likely start by adding your real data and writing
your custom `clean_data` function. Example is in `lib.py` file.

You may check the `__init__.py` for a proper way of accessing data
installed in `data` directory.

### Unit test

Basic unit test examples are to be found in `tests` directory.
Add your own when you add new functionalities. Unit tests
are run with coverage report, so we will know when you forgot to write them!

We insist that you don't ignore these tests. For Python, due to its
dynamic nature, unit tests are the only guarantee that your code
does what it is supposed to do. Good unit tests can save hours
of debugging time. Especially, if someone other than you will
work on your code.

### Scripts and functional tests

There is a very basic script in `scripts` directory. If you make
your own scripts please remember to add functional tests for them
too. For scripts it is usually sufficient to test that they run.
Correctness of answers should be covered by unit tests.

### Jupyter notebooks

Few jupyter notebooks that help you start with AKD tools are in
notebooks directory. To use them, your environment need to have
AKD tools installed.

Since AKD tools do not depend on your project, you may use them
independently. For example, if you have access to AKD tools on `fox@akd6`
you don't need to install you package there too. You
just need to provide data files.

Please do not add any of AKD tools as a dependency of your project,
unless you really need them for your library. If that's the case
contact 
[Luis \@luisarturo](https://zettafox.slack.com/team/U5YPEAM0U) or 
[Tomasz \@tomasz](https://zettafox.slack.com/team/U5ZFHJ1PB).

### Continuous integration and deployment

The package is configured with a basic CI/CD workflow
(see `.gitlab-ci.yml` file).
It shall pass when you create the project. You should
take care not to change that state. 
The CI/CD is here to help you spot your bugs!

### Note on handling dependencies

Any PyPI module that you package will depend on should be added
to `setup.py` file. In that way, it will be automatically
installed whenever you install the wheel of you package.
It is good practice to add version bounds too.

Requirements that are not in PyPI should be handled manually using
`requirements.txt` file.

Installation
------------

### Development

First, deactivate any virtual environment that may be active:

    $ deactivate

Install binary dependencies:

    $ sudo apt-get install python3 python3-pip python3-dev

Create new virtual environment and update pip and setup tools:

    $ python3 -m venv ~/venv
    $ source ~/venv/bin/activate
    $ pip install -U pip setuptools wheel

Optionally, install any non-PyPI requirements with:

    $ pip install -r requirements.txt

Install package for development (you don't need to reinstall if you
change the code):

    $ make dev_install

If you install on target system from sources use `make install` to build
a wheel and install it.

Check code quality (flake):

    $ make check_code

Run unit tests:

    $ make test

Functionnal test with a script:

    $ cd /tmp
    $ catchy_pkg-run

Continuous integration
----------------------

Every push of master branch will execute `.gitlab-ci.yml` docker jobs.
By default it will run tests, make a wheel and deploy it `kvm05` test server.

Bear in mind that Docker image used for deployment is very basic: it has
Ansible to run playbook and ssh to connect. 
Any builds in CI/CD workflow should be done in stages prior to deploy
with results registered as artifacts.

Production deployment
---------------------

Deployment on production should be done by CI/CD pipelines.
This ensures that tests pass and wheel is build successfully.

Before the first automatic deployment on new production environment you might
need to configure production environment properly. Use the
`ansible/prod_env.yml`
[Ansible](https://doc.zettafox.com/Tool_devops.html#ansible) playbook for that.

You will need to tweak the deployment options in `.gitlab-ci.yml` too:

- make a inventory for you production environment(s)
- possible tweak the virtual environment name with `--extra-vars "venv=name"`
  option.

Manual deployment is possible with Ansible playbook
`ansible/playbook_deploy.yml`, but you need to make a wheel first. Simplest use
case is to first build a wheel:

    $ make wheel

and then deploy with Ansible playbook:
    
    $ ansible-playbook -i ansible/all.serverlist ansible/playbook_deploy.yml

However, the case when you need a manual deployment is most probably the case
where default options will not work. Consult comments in playbook file for
details.

Please keep `prod_env.yml` and `playbook_deploy.yml` notebooks separate. Deploy
notebook should only copy and install wheels and optionally with some
post-install actions. It should not `apt install` or build anything.
