# -*- coding: UTF-8 -*-

# Import from standard library
import os
import catchy_pkg
import unittest

# Import from our lib
from catchy_pkg.lib import read_xlsx


class TestUtils(unittest.TestCase):

    # @unittest.skip('')
    def test_read_xlsx(self):
        datapath = os.path.dirname(os.path.abspath(catchy_pkg.__file__)) +\
                '/data'
        ourfile = '{}/comments.xlsx'.format(datapath)
        data = read_xlsx(ourfile)
        self.assertEqual(len(data), 35)


if __name__ == '__main__':
    unittest.main()
